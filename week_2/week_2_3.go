package main

import (
	"fmt"
)

type details interface {
	getName() string
	setName()
	avgMarks() float64
}

type student struct {
	name     string
	roll     int
	marks    []float64
	subjects int
}

func (s *student) getName() string {
	return s.name
}

func (s *student) setName(name string) {
	s.name = name
}

func (s *student) avgMarks() float64 {
	var total float64
	for _, i := range s.marks {
		total += i
	}
	return total / ((float64)(s.subjects))

}

func main() {
	s1 := student{
		"siddhesh",
		23,
		[]float64{67, 57, 86, 57},
		4,
	}
	Name := s1.getName()
	if Name == "siddhesh" {
		s1.setName("Avish")
	}
	avg := s1.avgMarks()
	fmt.Println("Average marks of", s1.getName(), "are", avg)
}
