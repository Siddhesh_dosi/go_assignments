package main

import (
	"fmt"
)

type employees struct {
	name       string
	age        int
	salary     float64
	city       string
	department string
	empId      int
}

func main() {
	var arr [5]employees
	arr[0] = employees{"Siddhesh", 21, 10000, "Jaipur", "GOlang", 5446}
	arr[1] = employees{"Avish", 22, 10000, "Banglore", "Java", 4583}
	arr[2] = employees{"Vaibhav", 21, 8000, "Jaipur", "HR", 1212}
	arr[3] = employees{"Rohit", 23, 15000, "Banglore", "Operation", 6767}
	arr[4] = employees{"Deependra", 21, 15000, "Jaipur", "Operation", 3464}

	for i := 0; i < len(arr); i++ {
		fmt.Println(arr[i])
	}
	fmt.Println()
}
