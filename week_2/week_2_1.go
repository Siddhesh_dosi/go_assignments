package main

import (
	"fmt"
)

func f() {

}

type employees struct {
	name       string
	age        int
	salary     float64
	city       string
	department string
	empId      int
}

func main() {

	emp1 := employees{"Siddhesh", 21, 10000, "Jaipur", "GOlang", 5446}
	emp2 := employees{"Avish", 22, 10000, "Banglore", "Java", 4583}
	emp3 := employees{"Vaibhav", 21, 8000, "Jaipur", "HR", 1212}
	emp4 := employees{"Rohit", 23, 15000, "Banglore", "Operation", 6767}
	fmt.Println("\nEmployees: ", emp1.name, emp2.name, emp3.name, emp4.name)
	fmt.Println("\nDetails ")
	fmt.Println("empi1", emp1)
	fmt.Println("empi1", emp2)
	fmt.Println("empi1", emp3)
	fmt.Println("empi1", emp4)
}
