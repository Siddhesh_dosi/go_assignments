package main

import (
	"fmt"
	"myPack/myPackDir"
)

func main() {
	var x, y int
	var op int
	fmt.Println("Enter Two numbers:")
	fmt.Scan(&x, &y)
	// fmt.Println(x, y)
	fmt.Println("Choose one option from below:")
	fmt.Println("1. ADD\n2. SUB\n3. MUL\n4. DIV")
	fmt.Scan(&op)
	// fmt.Println(x, y, op)
	switch op {
	case 1:
		fmt.Println(x, y)
		fmt.Println(myPackDir.Add(x, y))
	case 2:
		fmt.Println(x, y)
		fmt.Println(myPackDir.Sub(x, y))
	case 3:
		fmt.Println(x, y)
		fmt.Println(myPackDir.Mul(x, y))
	case 4:
		fmt.Println(x, y)
		fmt.Println(myPackDir.Div(x, y))
	}
}

// go mod init myPack
