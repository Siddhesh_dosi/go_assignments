package myPackDir

func Add(i, j int) int {
	result := i + j
	return result
}
func Sub(i, j int) int {
	result := i - j
	return result
}
func Mul(i, j int) int {
	result := i * j
	return result
}
func Div(i, j int) float32 {
	if j == 0 {
		return 0
	}
	result := float32(i / j)
	return result
}