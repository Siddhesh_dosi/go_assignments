package main

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

type Item struct {
	Title int
	Name  string
	Qnt   int
}

type ItemData struct {
	ItemTitle string
	Items     []Item
}

var data = ItemData{
	Items: []Item{},
}

func process(w http.ResponseWriter, r *http.Request) {
	// tmpl := template.Must(template.ParseFiles("layout.html"))
	r.ParseForm()
	fmt.Fprint(w, r.Form)
	num := 1
	for index, value := range r.Form {
		if value[0] == "on" {
			m, _ := strconv.Atoi(r.Form["qnt"+index][0])
			i := Item{num, index, m}
			data.Items = append(data.Items, i)
			num += 1
		}

	}
	fmt.Fprintln(w, data)
	// tmpl.Execute(w, data)

}

func cart(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("layout.html"))
	tmpl.Execute(w, data)
}

func listItems(w http.ResponseWriter, r *http.Request) {
	tmp2 := template.Must(template.ParseFiles("home.html"))
	products := []string{"Pen", "Eraser", "Pencil", "Book", "Notebook", "Scale"}
	var item = ItemData{}
	for _, n := range products {
		item.Items = append(item.Items, Item{Name: n})
	}
	fmt.Println(item)
	tmp2.Execute(w, item)
}
func main() {
	// fileServer := http.FileServer(http.Dir("./html_files"))
	// http.ListenAndServe(":8080", fileServer)
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}
	http.HandleFunc("/cart", cart)
	http.HandleFunc("/process", process)
	http.HandleFunc("/items", listItems)
	server.ListenAndServe()
}
