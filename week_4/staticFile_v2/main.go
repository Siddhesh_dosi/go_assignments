package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"text/template"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/gookit/color.v1"
)

// *************************************************************************
// Declaring collection
var collection *mongo.Collection

// Declaring context
var ctx = context.TODO()

// ------------------------------------------------------------------------

// *************************************************************************
// initialize the Database
func init() {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017/")
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	collection = client.Database("products").Collection("items")
}

// ------------------------------------------------------------------------

type Item struct {
	Title    int
	Name     string
	Qnt      int
	Selected bool
}

type ItemData struct {
	ItemTitle string
	Items     []Item
}

// *************************************************************************
// Creating a task in database 'products'
func createItem(item *Item) error {
	_, err := collection.InsertOne(ctx, item)
	return err
}

// ------------------------------------------------------------------------

// *************************************************************************
// Creating a filter which extract data
func filterTasks(filter interface{}) ([]*Item, error) {
	// A slice of tasks for storing the decoded documents
	var items []*Item

	cur, err := collection.Find(ctx, filter) // here filter is to match all the documents
	if err != nil {
		return items, err
	}

	for cur.Next(ctx) { // cur means cursor
		var t Item
		err := cur.Decode(&t)
		if err != nil {
			return items, err
		}

		items = append(items, &t)
	}

	if err := cur.Err(); err != nil {
		return items, err
	}

	// once exhausted, close the cursor
	cur.Close(ctx)

	if len(items) == 0 {
		return items, mongo.ErrNoDocuments
	}

	return items, nil
}

// ------------------------------------------------------------------------

// *************************************************************************
// func getAll() ([]*Item, error) {
// 	// passing bson.D{{}} matches all documents in the collection
// 	// binary encoded json documents are represented in a MongoDB database
// 	// By passing bson.D{{}} as your filter to filterTasks(),
// 	// you’re indicating that you want to match all the documents in the collection.
// 	filter := bson.D{{}}
// 	return filterTasks(filter)
// }

// ------------------------------------------------------------------------

// *************************************************************************
// get selected items
func getSelected() ([]*Item, error) {
	filter := bson.D{
		primitive.E{Key: "selected", Value: true},
	}
	return filterTasks(filter)
}

// ------------------------------------------------------------------------

// *************************************************************************
// print the items
func printTasks(items []*Item) {
	for i, v := range items {
		if v.Selected {
			color.Green.Printf("%d: %s\n", i+1, v.Name)
		} else {
			color.Yellow.Printf("%d: %s\n", i+1, v.Name)
		}
	}
}

// ------------------------------------------------------------------------

// *************************************************************************
// select the items
func selectTheItem(text string, quantity int) error {
	filter := bson.D{primitive.E{Key: "name", Value: text}}
	update := bson.D{primitive.E{Key: "$set", Value: bson.D{
		primitive.E{Key: "selected", Value: true},
		primitive.E{Key: "qnt", Value: quantity},
	}}}
	t := &Item{}
	// update
	return collection.FindOneAndUpdate(ctx, filter, update).Decode(t)
}

// ------------------------------------------------------------------------

// *************************************************************************
// deselect the item
func deselectTheItem(text string) error {
	filter := bson.D{primitive.E{Key: "name", Value: text}}
	update := bson.D{primitive.E{Key: "$set", Value: bson.D{
		primitive.E{Key: "selected", Value: false},
	}}}
	t := &Item{}
	return collection.FindOneAndUpdate(ctx, filter, update).Decode(t)
}

// ------------------------------------------------------------------------

func listItems(w http.ResponseWriter, r *http.Request) {
	tmp2 := template.Must(template.ParseFiles("home.html"))
	products := []string{"Pen", "Eraser", "Pencil", "Book", "Notebook", "Scale"}
	// products := []string{"Pen", "Pencil"}
	num := 1
	data := ItemData{
		ItemTitle: "Stationary",
		Items:     []Item{},
	}
	for _, n := range products {
		item := Item{Title: num, Name: n, Qnt: 0, Selected: false}
		data.Items = append(data.Items, item)
		//create the items
		filter := bson.D{
			primitive.E{Key: "name", Value: n},
		}
		_, err := filterTasks(filter)
		if err != nil {
			createItem(&item)
		} else {
			deselectTheItem(n)
		}
		num += 1
	}
	tmp2.Execute(w, data)
}
func fetchItemsInCart() ItemData {
	// fetch the data
	tasks, err := getSelected()
	if err != nil {
		if err == mongo.ErrNoDocuments {
			fmt.Print("Nothing to see here.")
		}
	}

	dataSelected := ItemData{
		ItemTitle: "Stationary1",
		Items:     []Item{},
	}

	for _, task := range tasks {
		t := *task
		dataSelected.Items = append(dataSelected.Items, t)
	}
	return dataSelected
}
func cart(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	for index, value := range r.Form {
		fmt.Println()
		if value[0] == "on" {
			quantity, _ := strconv.Atoi(r.Form["qnt"+index][0])
			if quantity == 0 {
				selectTheItem(index, 1)
			} else {
				// update
				selectTheItem(index, quantity)
			}
		}
	}
	tmpl := template.Must(template.ParseFiles("layout.html"))

	// fetch the data
	tasks, err := getSelected()
	if err != nil {
		if err == mongo.ErrNoDocuments {
			fmt.Print("Nothing to see here.")
		}
	}

	dataSelected := ItemData{
		ItemTitle: "Stationary1",
		Items:     []Item{},
	}

	for _, task := range tasks {
		t := *task
		dataSelected.Items = append(dataSelected.Items, t)
	}
	printTasks(tasks)
	tmpl.Execute(w, dataSelected)
}

func delete(w http.ResponseWriter, r *http.Request) {
	tmp2 := template.Must(template.ParseFiles("layout1.html"))
	tmp2.Execute(w, fetchItemsInCart())

}

func process(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	for index, value := range r.Form {
		fmt.Println()
		if value[0] == "on" {
			quantity, _ := strconv.Atoi(r.Form["qnt"+index][0])
			if quantity == 0 {
				selectTheItem(index, 0)
				deselectTheItem(index)
			} else {
				selectTheItem(index, quantity)
			}
		}
	}
	tmpl := template.Must(template.ParseFiles("layout.html"))
	tmpl.Execute(w, fetchItemsInCart())

	// call cart
}
func main() {
	// // fileServer := http.FileServer(http.Dir("./html_files"))
	// // http.ListenAndServe(":8080", fileServer)
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}
	http.HandleFunc("/items", listItems)
	http.HandleFunc("/process", process)
	http.HandleFunc("/cart", cart)
	http.HandleFunc("/delete", delete)
	server.ListenAndServe()

}
