package main

import (
	"fmt"
	"sync"
	"time"
)

var wait sync.WaitGroup
var count int
var mutex sync.Mutex

func increment() {
	for i := 0; i < 10; i++ {
		mutex.Lock()
		x := count
		x++
		time.Sleep(time.Millisecond * 100)
		count = x
		fmt.Println(count)
		mutex.Unlock()

	}
	wait.Done()

}
func main() {
	wait.Add(10)
	for i := 0; i < 10; i++ {
		go increment()
	}
	wait.Wait()
}
