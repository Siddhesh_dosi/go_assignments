package main

//1.	Write program to increment a number using value semantics as well as using pointer semantic.

import (
	"fmt"
)

func increment_1(i *int) {
	*i = *i + 1
}
func increment_2(i int) int {
	return i + 1
}

func main() {
	i := 10
	fmt.Println("Current value of i", i)
	increment_1(&i)
	fmt.Println("Value of i after call by pointer", i)
	i = increment_2(i)
	fmt.Println("Value of i after call by value", i)
}
