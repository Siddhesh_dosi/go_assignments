package config

import (
	"context"
	"doc_practice/assign_5_v1/static/entity"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// *************************************************************************
// Declaring Collection
var Collection *mongo.Collection

// Declaring context
var Ctx = context.TODO()

// *************************************************************************
// Creating a task in database 'products'
func CreateItem(item *entity.Item) error {
	_, err := Collection.InsertOne(Ctx, item)
	return err
}

// ------------------------------------------------------------------------

// *************************************************************************
// Creating a filter which extract data
func FilterTasks(filter interface{}) ([]*entity.Item, error) {
	// A slice of tasks for storing the decoded documents
	var items []*entity.Item

	cur, err := Collection.Find(Ctx, filter) // here filter is to match all the documents
	if err != nil {
		return items, err
	}
	num := 1
	for cur.Next(Ctx) { // cur means cursor
		var t entity.Item
		err := cur.Decode(&t)
		if err != nil {
			return items, err
		}
		t.Title = num
		items = append(items, &t)
		num += 1
	}

	if err := cur.Err(); err != nil {
		return items, err
	}

	// once exhausted, close the cursor
	cur.Close(Ctx)

	if len(items) == 0 {
		return items, mongo.ErrNoDocuments
	}

	return items, nil
}

// ------------------------------------------------------------------------

// *************************************************************************
// get selected items
func GetSelected() ([]*entity.Item, error) {
	filter := bson.D{
		primitive.E{Key: "selected", Value: true},
	}
	return FilterTasks(filter)
}

// ------------------------------------------------------------------------

// *************************************************************************

// ------------------------------------------------------------------------

// *************************************************************************
// select the items
func SelectTheItem(text string, quantity int) error {
	filter := bson.D{primitive.E{Key: "name", Value: text}}
	update := bson.D{primitive.E{Key: "$set", Value: bson.D{
		primitive.E{Key: "selected", Value: true},
		primitive.E{Key: "qnt", Value: quantity},
	}}}
	t := &entity.Item{}
	// update
	return Collection.FindOneAndUpdate(Ctx, filter, update).Decode(t)
}

// ------------------------------------------------------------------------

// *************************************************************************
// deselect the item
func DeselectTheItem(text string) error {
	filter := bson.D{primitive.E{Key: "name", Value: text}}
	update := bson.D{primitive.E{Key: "$set", Value: bson.D{
		primitive.E{Key: "selected", Value: false},
	}}}
	t := &entity.Item{}
	return Collection.FindOneAndUpdate(Ctx, filter, update).Decode(t)
}
