package entity

var Products = []string{"Pen", "Eraser", "Pencil", "Book", "Notebook", "Scale"}

type Item struct {
	Title    int
	Name     string
	Qnt      int
	Selected bool
	Price    float32
}

type ItemData struct {
	ItemTitle string
	Items     []Item
}
