package main

import (
	"doc_practice/assign_5_v1/static/config"
	"doc_practice/assign_5_v1/static/entity"
	"doc_practice/assign_5_v1/static/handlers"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// ------------------------------------------------------------------------

// *************************************************************************
// initialize the Database
func init() {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017/")
	client, err := mongo.Connect(config.Ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(config.Ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	config.Collection = client.Database("products").Collection("items")
	num := 1
	// data := entity.ItemData{
	// 	ItemTitle: "Stationary",
	// 	Items:     []entity.Item{},
	// }
	for _, n := range entity.Products {
		item := entity.Item{
			Title:    num,
			Name:     n,
			Qnt:      0,
			Selected: false,
		}
		filter := bson.D{
			primitive.E{Key: "name", Value: n},
		}
		_, err := config.FilterTasks(filter)
		if err != nil {
			config.CreateItem(&item)
		} else {
			config.SelectTheItem(n, 0)
			config.DeselectTheItem(n)
		}
		num += 1

	}
}

// ------------------------------------------------------------------------

// *************************************************************************
func main() {
	// // fileServer := http.FileServer(http.Dir("./html_files"))
	// // http.ListenAndServe(":8080", fileServer)
	// server := http.Server{
	// 	Addr: "127.0.0.1:8080",
	// }
	r := mux.NewRouter()
	r.HandleFunc("/items", handlers.ListItems)
	r.HandleFunc("/process", handlers.Process)
	r.HandleFunc("/cart", handlers.Cart)
	r.HandleFunc("/delete", handlers.Delete)
	http.ListenAndServe(":8080", r)
	// server.ListenAndServe()

}
