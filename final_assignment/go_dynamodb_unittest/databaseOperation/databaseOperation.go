package databaseOperation

import (
	"doc_practice/go_dynamodb_unittest/entity"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

// Function to create the session from localhost:8000
func CreateNewSession() (*dynamodb.DynamoDB, error) {
	sess, err := session.NewSession(&aws.Config{
		Endpoint: aws.String("http://localhost:8000"),
		Region:   aws.String("ap-south-1"),
	})
	if err != nil {
		log.Println(err)
		return nil, err
	}

	// Create DynamoDB client
	svc := dynamodb.New(sess)
	return svc, nil
}

// Listing all the existing tables from the database
func ListTables() ([]string, error) {

	svc, err := CreateNewSession()
	// ------------------------------------------------------------------------
	// error handling
	if err != nil {
		log.Print("Session not created")
		return nil, err
	}

	// create the input configuration instance
	input := &dynamodb.ListTablesInput{}

	log.Printf("Tables:\n")
	var tables []string
	for {
		// Get the list of tables
		result, err := svc.ListTables(input)
		// fmt.Print(result)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case dynamodb.ErrCodeInternalServerError:
					log.Println(dynamodb.ErrCodeInternalServerError, aerr.Error())
				default:
					log.Println(aerr.Error())
				}
			} else {
				// Print the error, cast err to awserr.Error to get the Code and
				// Message from an error.
				log.Println(err.Error())
			}
			return nil, err
		}

		for _, n := range result.TableNames {
			tables = append(tables, *n)
		}

		// assign the last read tablename as the start for our next call to the ListTables function
		// the maximum number of table names returned in a call is 100 (default), which requires us to make
		// multiple calls to the ListTables function to retrieve all table names
		input.ExclusiveStartTableName = result.LastEvaluatedTableName

		if result.LastEvaluatedTableName == nil {
			break
		}
	}
	return tables, nil
}

// Creating a table named Movies
func CreateTable() (string, error) {
	// Create table Movies

	// Create DynamoDB client
	svc, err := CreateNewSession()
	if err != nil {
		log.Print("Session not created")
		return "nil", err
		// ------------------------------------------------------------------------ handle not done
	}
	tableName := "Movies"
	/*
		// The partition key of an item is also known as its hash attribute.
		 The term "hash attribute" derives from DynamoDB's usage of an internal hash function to evenly distribute data items across partitions,
		  based on their partition key values.

		// The sort key of an item is also known as its range attribute. The term "range attribute"
		 derives from the way DynamoDB stores items with the same partition key physically close together,
		  in sorted order by the sort key value.
	*/
	input := &dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("Year"),
				AttributeType: aws.String("N"),
			},
			{
				AttributeName: aws.String("Title"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("Year"), // partition key
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("Title"), // sort key
				KeyType:       aws.String("RANGE"),
			},
		},
		/*
			The maximum number of strongly consistent reads/write consumed per second before DynamoDB returns a ThrottlingException.
		*/
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		TableName: aws.String(tableName),
	}

	// This command will create the table whose specifications are defined by the input attribute
	_, err = svc.CreateTable(input)
	if err != nil {
		log.Fatalf("Got error calling CreateTable: %s", err)
		return "nil", &json.SyntaxError{}
	}

	log.Println("Created the table", tableName)
	return tableName, nil
}

// Inserting items directly using struct
func InsertItem(item entity.Item) (string, error) {
	fmt.Println("Inserting..")
	// Create DynamoDB client
	svc, err := CreateNewSession()
	if err != nil {
		log.Println("Error occured", err)
		return "", err
	}
	// ------------------------------------------------------------------------
	// Create a struct with the movie data and marshall that data into a map of AttributeValue objects.
	// item := entity.Item{
	// 	Year:   2015,
	// 	Title:  "The Big New Movie",
	// 	Plot:   "Nothing happens at all.",
	// 	Rating: 0.0,
	// }

	av, err := dynamodbattribute.MarshalMap(item)
	if err != nil {
		// log.Fatalf("Got error marshalling new movie item: %s", err)
		log.Printf("Got error marshalling new movie item: %s", err)
		return "", err
	}

	// ------------------------------------------------------------------------
	// Create the input for PutItem and call it.
	// Create item in table Movies
	tableName := "Movies"

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
	}

	_, err = svc.PutItem(input)
	if err != nil {
		log.Printf("Got error calling PutItem: %s", err)
		return "", err
	}

	year := strconv.Itoa(item.Year)

	logString := "Successfully added '" + item.Title + "' (" + year + ") to table " + tableName
	log.Println(logString)
	return logString, err
}

// converting json attributes to Item struct
func GetItemsFromJson() []entity.Item {
	raw, err := ioutil.ReadFile("./movie_data.json")
	if err != nil {
		log.Fatalf("Got error reading file: %s", err)
	}

	var items []entity.Item
	json.Unmarshal(raw, &items)
	return items
}

// Inserting items using json file
func InsertItemJson() ([]string, error) {
	svc, err := CreateNewSession()
	if err != nil {
		log.Println("Error occured while creating session", err)
		return nil, err
	}
	// if svc == nil {
	// 	print("Session creationg error")
	// }
	// Get table items from .movie_data.json
	items := GetItemsFromJson()

	// Add each item to Movies table:
	tableName := "Movies"

	var logString []string
	for _, item := range items {
		av, err := dynamodbattribute.MarshalMap(item)
		if err != nil {
			log.Printf("Got error marshalling map: %s", err)
			return nil, err
		}

		// Create item in table Movies
		input := &dynamodb.PutItemInput{
			Item:      av,
			TableName: aws.String(tableName),
		}

		_, err = svc.PutItem(input)
		if err != nil {
			log.Printf("Got error calling PutItem: %s", err)
			return nil, err
		}

		year := strconv.Itoa(item.Year)

		logString = append(logString, "Successfully added '"+item.Title+"' ("+year+") to table "+tableName)
		log.Println(logString)
	}
	return logString, nil
}

// Fetching the data from the database
func RetriveItem(movieName, movieYear string) (entity.Item, error) {
	fmt.Println(movieName, movieYear)
	//  Retriving using year and tile as a combined key
	svc, err := CreateNewSession()
	if err != nil {
		log.Println("Error occured while creating session", err)
		return entity.Item{}, err
	}
	// ------------------------------------------------------------------------
	// Call GetItem to get the item from the table.
	tableName := "Movies"
	// movieName := "Turn It Down, Or Else!"
	// movieYear := "2015"

	input := &dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"Year": {
				N: aws.String(movieYear),
			},
			"Title": {
				S: aws.String(movieName),
			},
		},
	}
	result, err := svc.GetItem(input)
	if err != nil {
		log.Printf("Got error calling GetItem: %s", err)
		return entity.Item{}, err
	}

	// ------------------------------------------------------------------------
	// If an item was returned, unmarshall the return value and display the year, title, plot, and rating.
	if result.Item == nil {
		msg := "Could not find '" + movieName + "'"
		log.Println(errors.New(msg))
		return entity.Item{}, errors.New(msg)
	}

	item := entity.Item{}

	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		panic(fmt.Sprintf("Failed to unmarshal Record, %v", err))
	}

	// fmt.Println("Found item:")
	// fmt.Println("Year:  ", item.Year)
	// fmt.Println("Title: ", item.Title)
	// fmt.Println("Plot:  ", item.Plot)
	// fmt.Println("Rating:", item.Rating)
	return item, err
}

func RetriveItemUsingExpression(minRating float64, year int) ([]entity.Movies, error) {
	svc, err := CreateNewSession()
	if err != nil {
		log.Println("Error occured while creating session", err)
		return []entity.Movies{}, err
	}
	// ------------------------------------------------------------------------
	// Create variables for the minimum rating and year for the table items to retrieve.
	tableName := "Movies"
	// minRating := 4.0
	// year := 2013

	// ------------------------------------------------------------------------
	// Create the expression defining the year for which we filter the table items to retrieve,
	//  and the projection so we get the title, year, and rating for each retrieved item.
	// Create the Expression to fill the input struct with.
	// Get all movies in that year; we'll pull out those with a higher rating later
	filt := expression.Name("Year").Equal(expression.Value(year))

	// Or we could get by ratings and pull out those with the right year later
	//    filt := expression.Name("info.rating").GreaterThan(expression.Value(min_rating))

	// Get back the title, year, and rating
	proj := expression.NamesList(expression.Name("Title"), expression.Name("Year"), expression.Name("Rating"))

	expr, err := expression.NewBuilder().WithFilter(filt).WithProjection(proj).Build()
	if err != nil {
		log.Printf("Got error building expression: %s", err)
		return []entity.Movies{}, err
	}

	// ------------------------------------------------------------------------
	// Create the inputs for and call Scan to retrieve the items from the table (the movies made in 2013).
	// Build the query input parameters
	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(tableName),
	}

	// Make the DynamoDB Query API call
	result, err := svc.Scan(params)
	if err != nil {
		log.Printf("Query API call failed: %s", err)
		return []entity.Movies{}, err
	}

	// ------------------------------------------------------------------------
	// Loop through the movies from 2013 and display the title and rating for those where the rating is greater than 4.0
	numItems := 0
	var movies []entity.Movies
	for _, i := range result.Items {
		item := entity.Item{}

		err = dynamodbattribute.UnmarshalMap(i, &item)

		if err != nil {
			log.Printf("Got error unmarshalling: %s", err)
			return []entity.Movies{}, err
		}

		// Which ones had a higher rating than minimum?
		if item.Rating > minRating {
			// Or it we had filtered by rating previously:
			//   if item.Year == year {
			numItems++
			movie := entity.Movies{
				Title:  item.Title,
				Rating: item.Rating,
			}
			// fmt.Println("Title: ", item.Title)
			// fmt.Println("Rating:", item.Rating)
			// fmt.Println()
			movies = append(movies, movie)
		}
	}

	log.Println("Found", numItems, "movie(s) with a rating above", minRating, "in", year)
	return movies, nil
}

func UpdateItem(item entity.Item) (string, error) {
	svc, err := CreateNewSession()
	if err != nil {
		log.Println("Error occured while creating session", err)
		return "", err
	} // Update item in table Movies

	tableName := "Movies"
	movieName := item.Title
	movieYear := strconv.Itoa(item.Year)
	movieRating := strconv.FormatFloat(item.Rating, 'f', -1, 32)

	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":r": {
				N: aws.String(movieRating),
			},
		},
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"Year": {
				N: aws.String(movieYear),
			},
			"Title": {
				S: aws.String(movieName),
			},
		},
		ReturnValues:     aws.String("UPDATED_NEW"),
		UpdateExpression: aws.String("set Rating = :r"),
	}

	_, err = svc.UpdateItem(input)
	if err != nil {
		log.Printf("Got error calling UpdateItem: %s", err)
		return "", err
	}

	logString := "Successfully updated '" + movieName + "' (" + movieYear + ") rating to " + movieRating
	log.Println(logString)

	return logString, nil

}
func DeleteItem(movieName, movieYear string) (string, error) {
	svc, err := CreateNewSession()
	if err != nil {
		log.Println("Error occured while creating session", err)
		return "", err
	}
	tableName := "Movies"
	// movieName := "The Big New Movie"
	// movieYear := "2015"

	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"Year": {
				N: aws.String(movieYear),
			},
			"Title": {
				S: aws.String(movieName),
			},
		},
		TableName: aws.String(tableName),
	}

	_, err = svc.DeleteItem(input)
	if err != nil {
		log.Printf("Got error calling DeleteItem: %s", err)
		return "", nil
	}

	deleteLog := "Deleted '" + movieName + "' (" + movieYear + ") from table " + tableName
	log.Println(deleteLog)
	return deleteLog, nil

}
