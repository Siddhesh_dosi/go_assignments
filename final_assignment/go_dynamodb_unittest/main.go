package main

import (
	"doc_practice/go_dynamodb_unittest/handlers"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {

	// databaseOperation.CreateTable()
	// databaseOperation.ListTables()
	// databaseOperation.InsertItem()
	// databaseOperation.InsertItemJson()
	// databaseOperation.RetriveItem()
	// databaseOperation.RetriveItemUsingExpression()
	// databaseOperation.UpdateItem()
	// databaseOperation.DeleteItem()

	// ------------------------------------------------------------------------
	r := mux.NewRouter()

	//  Create ----------------
	// To Render the insert item html page
	r.HandleFunc("/listAllTables", handlers.ListTables).Methods(http.MethodGet)

	r.HandleFunc("/insertMultiItems", handlers.CreateEntryMany).Methods(http.MethodPost)
	r.HandleFunc("/insertSingleItem", handlers.CreateEntryOne).Methods(http.MethodPost)

	r.HandleFunc("/retriveItem/{movie}/{year}", handlers.GetEntry).Methods(http.MethodGet)
	r.HandleFunc("/retriveIf/{minRate}/{year}", handlers.GetEntryOn).Methods(http.MethodGet)

	r.HandleFunc("/updateItem/{movie}/{year}", handlers.UpdateEntry).Methods(http.MethodPut)

	r.HandleFunc("/deleteItemJson", handlers.DeleteEntryUsingJson).Methods(http.MethodDelete)
	r.HandleFunc("/deleteItem/{movie}/{year}", handlers.DeleteEntry).Methods(http.MethodDelete)

	http.ListenAndServe(":8080", r)
	// server.ListenAndServe()

	// fmt.Println("Hello")
}
