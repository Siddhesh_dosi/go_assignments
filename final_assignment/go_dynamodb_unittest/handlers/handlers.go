package handlers

import (
	"doc_practice/go_dynamodb_unittest/databaseOperation"
	"doc_practice/go_dynamodb_unittest/entity"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	// "sync"

	"github.com/gorilla/mux"
)

// var wait sync.WaitGroup

// var mutex sync.Mutex

// ###########################################################################
// RespondWithError is called on an error to return info regarding error
func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

// Called for responses to encode and send json data
func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	//encode payload to json
	response, _ := json.Marshal(payload)

	// set headers and write response
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

// ############################################################################

// *************************************************************************

// To create the database documents
func ListTables(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint: ListTables")
	tables, err := databaseOperation.ListTables()
	if err != nil {
		log.Print("Cannot fetch tables!")
		respondWithError(w, http.StatusInternalServerError, "Something went wrong.")
	}
	print(tables)
	// fmt.Fprint(w, tables)
	respondWithJSON(w, http.StatusOK, tables)

}

// *************************************************************************

// To create the database documents
func GetEntry(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint: Get single entry")
	vars := mux.Vars(r)
	year := vars["year"]
	movie := vars["movie"]
	// log.Println(year, movie)
	item, err := databaseOperation.RetriveItem(movie, year)
	if err.Error() == "Could not find '"+movie+"'" {
		respondWithError(w, http.StatusNotFound, "We found 0 items")
	} else if err != nil {
		log.Println("Error while retriving entry on", movie, " of ", year)
		respondWithError(w, http.StatusInternalServerError, "Something went wrong.")

	} else {
		respondWithJSON(w, http.StatusOK, item)
	}

	// if err != nil {
	// 	log.Println("Error while retriving entry on", movie, " of ", year)
	// 	respondWithError(w, http.StatusInternalServerError, "Something went wrong.")

	// }
	// // json.NewEncoder(w).Encode(item)
	// // fmt.Fprint(w, item)
	// respondWithJSON(w, http.StatusOK, item)

}

// *************************************************************************

// To create the database documents
func UpdateEntry(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint: update entry")
	vars := mux.Vars(r)
	year, _ := strconv.Atoi(vars["year"])
	movie := vars["movie"]
	reqBody, _ := ioutil.ReadAll(r.Body)
	var newItem entity.Item
	json.Unmarshal(reqBody, &newItem)
	if newItem.Title == movie && newItem.Year == year {
		updateLog, err := databaseOperation.UpdateItem(newItem)
		if err != nil {
			log.Println("Error while updating: ", newItem)
			respondWithError(w, http.StatusInternalServerError, "Something went wrong.")
		}
		respondWithJSON(w, http.StatusOK, updateLog)
	} else {
		log.Println("Movie name and json data mismatch")
		respondWithError(w, http.StatusInternalServerError, "Something went wrong.")
	}

}

// *************************************************************************

// To create the database documents
func GetEntryOn(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint: get entry with relative condition")
	vars := mux.Vars(r)
	year, _ := strconv.Atoi(vars["year"])
	min_rating, _ := strconv.ParseFloat(vars["minRate"], 64)
	//  := strconv.FormatFloat, 'f', -1, 64)
	movies, err := databaseOperation.RetriveItemUsingExpression(min_rating, year)
	if err != nil {
		log.Println("Error while getting entry on", min_rating, " of ", year)
		respondWithError(w, http.StatusInternalServerError, "Something went wrong.")
	}
	if len(movies) == 0 {
		respondWithError(w, http.StatusNotFound, "We found 0 items")
	} else {
		respondWithJSON(w, http.StatusOK, movies)
	}
}

// *************************************************************************

// To create the database documents
func CreateEntryMany(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint: Create many entrie")
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("Error occured while reading data from request body", err)
		respondWithError(w, http.StatusInternalServerError, "Something went wrong.")
	}
	var item entity.Item
	var itemList []entity.Item
	json.Unmarshal(reqBody, &itemList)
	var insertLogs []string
	ch := make(chan string)

	// wait.Add(len(itemList))

	for i := 0; i < len(itemList); i++ {
		item = itemList[i]

		// *************************************************************************
		// used the concept of go routines and closure functions
		go func(chan string) {
			insertLog, err := databaseOperation.InsertItem(item)
			if err != nil {
				log.Println("Error occured while inserting ", item, err)
				respondWithError(w, http.StatusInternalServerError, "Something went wrong.")
			}
			ch <- insertLog
			// wait.Done()
		}(ch)

		insertLogs = append(insertLogs, <-ch)

		// wait.Wait()

	}
	respondWithJSON(w, http.StatusOK, insertLogs)
}

// *************************************************************************

// To create the database documents
func CreateEntryOne(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint: Create one entry")
	log.Println("Endpoint: ListTables")
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("Error occured while reading data from request body", err)
		respondWithError(w, http.StatusInternalServerError, "Something went wrong.")
	}
	var item entity.Item
	json.Unmarshal(reqBody, &item)
	insertLog, err := databaseOperation.InsertItem(item)
	if err != nil {
		log.Println("Error while creating entry in database", err, item)
		respondWithError(w, http.StatusInternalServerError, "Something went wrong.")
	}
	respondWithJSON(w, http.StatusOK, insertLog)

}

//  To delete the database documents
func DeleteEntry(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint: Delete single entry")
	vars := mux.Vars(r)
	year := vars["year"]
	movie := vars["movie"]
	deleteLog, err := databaseOperation.DeleteItem(movie, year)
	if err != nil {
		log.Println("error while deleting", err, "item: ", movie, year)
		respondWithError(w, http.StatusInternalServerError, "Something went wrong.")
	}
	respondWithJSON(w, http.StatusOK, deleteLog)

}

// *************************************************************************

//  To delete the database documents
func DeleteEntryUsingJson(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint: Delete many entries")
	reqBody, _ := ioutil.ReadAll(r.Body)
	var key entity.Key
	var keyList []entity.Key
	json.Unmarshal(reqBody, &keyList)
	ch := make(chan string)
	// wait.Add(len(keyList))
	var deleteLogs []string
	for i := 0; i < len(keyList); i++ {
		key = keyList[i]
		go func(chan string) {
			deleteLog, err := databaseOperation.DeleteItem(key.Title, strconv.Itoa(key.Year))
			if err != nil {
				log.Println("Error occured while deleting ", key, err)
				respondWithError(w, http.StatusInternalServerError, "Something went wrong.")
			}
			ch <- deleteLog
			// wait.Done()
		}(ch)
		deleteLogs = append(deleteLogs, <-ch)
		// wait.Wait()
	}
	respondWithJSON(w, http.StatusOK, deleteLogs)

}

// *************************************************************************
