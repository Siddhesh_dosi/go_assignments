package main

import (
	"bytes"
	"doc_practice/go_dynamodb_unittest/handlers"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
)

func callHandlers(req *http.Request, t *testing.T, handlerName func(http.ResponseWriter, *http.Request), expectedString string, status int) {
	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(handlerName)
	handler.ServeHTTP(responseRecorder, req)
	if status1 := responseRecorder.Code; status1 != status {
		t.Errorf("handler returned wrong status code: got %v want %v", status1, status)
	}
	if responseRecorder.Body.String() != expectedString {
		t.Errorf("handler returned unexpected body: got %v want %v",
			responseRecorder.Body.String(), expectedString)
	}
}

func TestListTables(t *testing.T) {
	expected := `["Movies"]`
	req, err := http.NewRequest("GET", "/listAllTables", nil) // return http.request
	if err != nil {
		t.Fatal(err)
	}
	callHandlers(req, t, handlers.ListTables, expected, http.StatusOK)
}

func TestCreateEntryMany(t *testing.T) {
	expected := `["Successfully added 'Dangal' (2014) to table Movies","Successfully added 'Sultan' (2014) to table Movies"]`

	var jsonStr = []byte(`[ {
        "Year": 2014,
        "Title": "Dangal",
        "Plot": "Based on true story of wrestles",
        "Rating": 4.0
    },
        {
        "Year" : 2014,
        "Title" : "Sultan",
        "Plot" : "Drama action on a wrestler to boxing champion",
        "Rating" : 4.5
    }
]`)

	req, err := http.NewRequest("POST", "/insertMultiItems", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	callHandlers(req, t, handlers.CreateEntryMany, expected, http.StatusOK)
}

func TestCreateEntryOne(t *testing.T) {
	expected := `"Successfully added 'Movie XYZ' (2015) to table Movies"`

	var jsonStr = []byte(`        {
        "Year" : 2015,
        "Title" : "Movie XYZ",
        "Plot" : "Some Indian drama",
        "Rating" : 3.5
    }`)

	req, err := http.NewRequest("POST", "/insertSingleItem", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	callHandlers(req, t, handlers.CreateEntryOne, expected, http.StatusOK)
}

func TestGetEntry(t *testing.T) {
	expected := `{"Year":2015,"Title":"Movie XYZ","Plot":"Some Indian drama","Rating":3.5}`

	req, err := http.NewRequest("GET", "/retriveItem/Movie XYZ/2015", nil)
	if err != nil {
		t.Fatal(err)
	}
	vars := map[string]string{
		"movie": "Movie XYZ",
		"year":  "2015",
	}

	// CHANGE THIS LINE!!!
	req = mux.SetURLVars(req, vars)
	// q := req.URL.Query()
	// q.Add("movie", "Movie XYZ")
	// q.Add("year", "2015")
	// req.URL.RawQuery = q.Encode()
	callHandlers(req, t, handlers.GetEntry, expected, http.StatusOK)
}
func TestGetEntryOn(t *testing.T) {
	expected := `[{"Title":"Dangal","Rating":4},{"Title":"Sultan","Rating":4.5}]`

	req, err := http.NewRequest("GET", "/retriveIf/3.0/2014", nil)
	if err != nil {
		t.Fatal(err)
	}
	vars := map[string]string{
		"minRate": "3.0",
		"year":    "2014",
	}
	req = mux.SetURLVars(req, vars)
	callHandlers(req, t, handlers.GetEntryOn, expected, http.StatusOK)
}

func TestUpdateEntry(t *testing.T) {
	expected := `"Successfully updated 'Movie XYZ' (2015) rating to 4"`

	var jsonStr = []byte(`       {
		"Year": 2015,
		"Title": "Movie XYZ",
		"Plot": "Sci-fi.",
		"Rating": 4.0
}`)
	req, err := http.NewRequest("PUT", "/updateItem/Movie XYZ/2015", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}

	vars := map[string]string{
		"movie": "Movie XYZ",
		"year":  "2015",
	}
	req = mux.SetURLVars(req, vars)
	callHandlers(req, t, handlers.UpdateEntry, expected, http.StatusOK)
}

func TestDeleteEntryUsingJson(t *testing.T) {
	expected := `["Deleted 'Dangal' (2014) from table Movies","Deleted 'Sultan' (2014) from table Movies"]`

	var jsonStr = []byte(`       [
		{
			"Year": 2014,
			"Title": "Dangal"
		},
		{
			"Year": 2014,
			"Title": "Sultan"
		}
	]`)
	req, err := http.NewRequest("DELETE", "/deleteItemJson", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	callHandlers(req, t, handlers.DeleteEntryUsingJson, expected, http.StatusOK)
}

// Test case if Item not found with condition
func TestGetEntryOnNotFound(t *testing.T) {
	expected := `{"error":"We found 0 items"}`

	req, err := http.NewRequest("GET", "/retriveIf/3.0/2014", nil)
	if err != nil {
		t.Fatal(err)
	}
	vars := map[string]string{
		"minRate": "3.0",
		"year":    "2014",
	}
	req = mux.SetURLVars(req, vars)
	callHandlers(req, t, handlers.GetEntryOn, expected, http.StatusNotFound)
}

// Test case to delete single item
func TestDeleteEntry(t *testing.T) {
	expected := `"Deleted 'Movie XYZ' (2015) from table Movies"`

	req, err := http.NewRequest("DELETE", "/deleteItem/Movie XYZ/2015", nil)
	if err != nil {
		t.Fatal(err)
	}
	vars := map[string]string{
		"movie": "Movie XYZ",
		"year":  "2015",
	}
	req = mux.SetURLVars(req, vars)
	callHandlers(req, t, handlers.DeleteEntry, expected, http.StatusOK)
}

// Test case if Item not found
func TestGetEntryNotFound(t *testing.T) {
	expected := `{"error":"We found 0 items"}`

	req, err := http.NewRequest("GET", "/retriveIf/Movie XYZ/2015", nil)
	if err != nil {
		t.Fatal(err)
	}
	vars := map[string]string{
		"movie": "Movie XYZ",
		"year":  "2015",
	}
	req = mux.SetURLVars(req, vars)
	callHandlers(req, t, handlers.GetEntry, expected, http.StatusNotFound)
}
 