package entity

// Create struct to hold info about new item
type Item struct {
	Year   int
	Title  string
	Plot   string
	Rating float64
}

type Movies struct {
	Title  string
	Rating float64
}
type Key struct {
	Title string
	Year  int
}
type ItemData struct {
	ItemTitle string
	Items     []Item
}
