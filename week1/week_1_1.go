// This is a sample file
package main

import "fmt"

func main() {
	for i := 0; i < 5; i++ {
		for j := 0; j < 4-i; j++ {
			fmt.Print(" ")
		}
		m := i + 1
		for k := 0; k <= i; k++ {
			fmt.Print(m)
			m--
		}
		m++
		for n := 0; n < i; n++ {
			m++
			fmt.Print(m)
		}
		fmt.Println()
	}
}
