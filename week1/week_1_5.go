package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

var (
	newFile *os.File
	err     error
)

func createFIle() {
	// Create() will return two parameters 1> file pointer and 2> error, if cannot created
	newFile, err = os.Create("NOTES.txt")
	// err must be "nil" is created properly
	if err != nil {
		log.Fatal(err)
	}
	// To print the output log at the terminal
	log.Println(newFile)
	// Close() will close the file
	newFile.Close()
}

func writeFIle() {
	newFile, err = os.OpenFile("NOTES.txt", os.O_RDWR, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer newFile.Close()
	for i := 1; i <= 100; i++ {
		_, err := newFile.WriteString(fmt.Sprintf("%d ", i))
		if err != nil {
			log.Fatal(err)
		}
	}
}

func readFile() {
	data, err := ioutil.ReadFile("NOTES.txt")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Contents of file:", string(data))
}
func main() {

	createFIle()
	writeFIle()
	readFile()
}
