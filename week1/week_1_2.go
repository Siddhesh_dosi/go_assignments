package main

import "fmt"

//`Write a program that accepts 6 real numbers from the keyboard and prints
// out the difference of the maximum and minimum values of these numbers.

func main() {
	var arr [6]int
	var min int = 1<<32 - 1
	var max int = (1<<32 - 1) * -1
	fmt.Printf("Initial max: %d \nInitial min: %d\n", max, min)
	for i := 0; i < 6; i++ {
		fmt.Print("Enter number: ")
		fmt.Scanln(&arr[i])
		if arr[i] > max {
			max = arr[i]
		}
		if arr[i] < min {
			min = arr[i]
		}
	}
	fmt.Println(max - min)

}
