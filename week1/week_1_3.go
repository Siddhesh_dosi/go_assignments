package main

import "fmt"

//. Write a program to sum of digits of given integer number

func main() {
	var integer int
	fmt.Scanf("%d", &integer)
	var Sum int = 0
	var rem int
	for integer > 0 {
		rem = integer % 10
		integer /= 10
		Sum += rem
	}
	fmt.Println(Sum)

}
