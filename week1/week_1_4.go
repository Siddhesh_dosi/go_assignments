//Write a Go program to find the sum and average of one-dimensional integer array.
//Swap the elements in the array and print it on the screen.
// Sort the array.

package main

import (
	"fmt"
	"sort"
)

func main() {
	var n int
	fmt.Println("Enter the length of the array")
	fmt.Scanf("%d", &n)
	fmt.Println("Enter numbers in newlines", n)
	arr := make([]int, n)

	/// Task1 : calc avg
	var total int = 0
	for i := 0; i < n; i++ {
		fmt.Scan(&arr[i])
		total += arr[i]
	}
	avg := total / n
	fmt.Println("Avg ", avg)
	fmt.Println(arr)

	////// Task2 : swap in array
	fmt.Println("Enter two indexes to swap")
	var index1, index2 int
	fmt.Scan(&index1)
	fmt.Scan(&index2)

	/// swapping
	arr[index1] += arr[index2]
	arr[index2] = arr[index1] - arr[index2]
	arr[index1] = arr[index1] - arr[index2]
	fmt.Println("Array after swap", arr)

	sort.Ints(arr)

	fmt.Println("Sorted array", arr)

}
